
class user{
    public userId: number;
    public name: string;
    public password: string;
    public initialBalance: number;
    constructor(userId:number ,name:string,password:string,initialBalance: number ){
        this.userId = userId;
        this.name = name;
        this.password = password;
        this.initialBalance = initialBalance;
    }
}

class account extends user{
    public accountId: number;
    public balance: number;
    public depositAmount: number;
    public transferAmount: number;
    public withdrawAmount: number;
    constructor(userId: number,
        initialBalance: number,
        password: string,
        name: string,
        accountId: number, 
        balance: number, 
        depositAmount: number,
        transferAmount: number,
        withdrawAmount: number){
        super(userId, name, password, initialBalance);

        this.userId = accountId;
        this.initialBalance = balance;
        this.password= password;
        this.name= name;
        this.depositAmount= depositAmount;
        this.transferAmount = transferAmount;
        this.withdrawAmount = withdrawAmount;
    }

    static withdraw =(initialBalance: number, withdrawAmount: number) :number => {
        if(withdrawAmount <= initialBalance){
            initialBalance -= withdrawAmount;
            return  initialBalance;
        }
    }

    static  deposit = (initialAmount: number, depositAmount: number) =>{
        initialAmount += depositAmount
        return  initialAmount;
    }

    static  transfer = (transferamount: number, firstuserbalance: number, currentuserbalance: number) => {
        currentuserbalance -= transferamount;
        firstuserbalance += transferamount;
        return [currentuserbalance, firstuserbalance];
    }
}



const ian : user = new  user(1, "ian", "123456" , 50000  )
const carren :user = new user(2,"carren", "cann", 1000);

let users : user[] = [ian, carren];

const getusers = () : user[] => users;

const getfirstuser = () : user => {
    for(let i = 0; i < users.length; i++){
        if(users[i].name == "ian" && users[i].password == "123456"){
         const  firstuser = users[i];
         return  firstuser;
        }
    }
}
const getlastuser = () : user => {
    for (let i = 0; i < users.length; i++) {
        if(users[i].name == "carren" && users[i].password == "cann"){
            const lastuser = users[i];
            return lastuser;
        }
        
    }
}



const createnewuser = (name: string,password: string,initialBalance: number ): user =>{
        const   previoususer:  user = getlastuser();
        const   newuserId: number = previoususer.userId +1;
        const   newuser: user = new user(
            newuserId,
            name,
            password,
            initialBalance
        );
        adduser(newuser);
        return newuser;
}

const   validateUser = ( candidateuser: user, previoususer:user) : boolean => {
    if(candidateuser.userId == previoususer.userId+ 1){
        return true;
    }else{
        return  false;
    }
}

const adduser =(candidateuser: user) : void =>{
    if(validateUser(candidateuser, getlastuser())){
        users.push(candidateuser);
    }
    
}





/*account methods and functions */

const   withdrawMoney=(amounttowithdraw: number): number => {
    const  currentuser  = getlastuser();
    const currentBalance   = currentuser.initialBalance;
    const   newBalance = account.withdraw(currentBalance, amounttowithdraw);
    currentuser.initialBalance = newBalance;
    return newBalance;
}

const   depositMoney=(amounttodeposit: number): number => {
    const  currentuser  = getlastuser();
    const currentBalance   = currentuser.initialBalance;
    const   newBalance = account.deposit(currentBalance, amounttodeposit);
    currentuser.initialBalance = newBalance;
    return newBalance;
}

const   transferMoney =(amounttotransfer: number): number[]   => {
    const firstuser = getfirstuser();
    const  currentuser  = getlastuser();
    const   firstuserBalance   = firstuser.initialBalance;
    const currentBalance   = currentuser.initialBalance;
    const   newBalance = account.transfer(amounttotransfer, firstuserBalance, currentBalance);
    currentuser.initialBalance = newBalance[0];
    firstuser.initialBalance    = newBalance[1];
    return newBalance;

}

// console.log(depositMoney(600));
// console.log(withdrawMoney(300));
// console.log(transferMoney(400));
// console.log(users);

export{};